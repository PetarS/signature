<div class="container login-container">    
    <div id="loginbox" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">                    
        
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="panel-title">Enter Username and Password</div>
            </div>     

            <div class="panel-body" style="text-align: center;">


                <form class="form-horizontal login-form" role="form">

                    <div class="input-group mar-top">
                        <span class="input-group-addon user"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="Username">                                        
                    </div>

                    <div class="input-group mar-top-l">
                        <span class="input-group-addon key"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Password">
                    </div>



                    <div class="mar-top-l">
                        <a id="btn-login" href="#" class="btn btn-danger">Login </a>

                    </div> 
                </form>     



            </div>                     
        </div>  
    </div>

</div>

