<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>RedBarMedia</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url("css/bootstrap.min.css") ?>" rel="stylesheet"/>
        <!-- Select -->
        <link href="<?php echo base_url("css/bootstrap-select.css") ?>" rel="stylesheet"/>
        <!-- Input type file -->
        <link href="<?php echo base_url("css/fileinput.css") ?>" rel="stylesheet"/>
        <!-- font Awesome -->
        <link href="<?php echo base_url("css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url("css/ionicons.min.css") ?>" rel="stylesheet" type="text/css" />

        <!-- Custom -->
        <link href="<?php echo base_url("css/style.css") ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery 2.1.1 -->
        <script src="<?php echo base_url("js/jquery-2.1.1.min.js") ?>"></script>
        
        <!--<link rel="icon" type="img/ico" href="<?php echo base_url("img/favicon.ico") ?>">-->
    </head>
    <body>

