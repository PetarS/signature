<div class="container content-holder">
    <a href="<?php echo base_url("") ?>" class="logo"><img src="<?php echo base_url("img/logo.png") ?>" ></a>

    
   

    <div class="bit-data">
        <div class="bit-data-holder">
            <table>
                <tr class="up data-atms"><td>328</td></tr>
                <tr class="down"><td>ATMs<span class="data-arrow"></span></td></tr>
            </table>
        </div>
        <div class="bit-data-holder">
            <table>
                <tr class="up data-groups"><td>13</td></tr>
                <tr class="down"><td>Groups<span class="data-arrow"></span></td></tr>
            </table>
        </div>
        <div class="bit-data-holder">
            <table>
                <tr class="up data-clients"><td>10</td></tr>
                <tr class="down"><td>Clients<span class="data-arrow"></span></td></tr>
            </table>
        </div>
        <div class="bit-data-holder">
            <table>
                <tr class="up data-promos"><td>35</td></tr>
                <tr class="down"><td>Promos<span class="data-arrow"></span></td></tr>
            </table>
        </div>
        <div class="bit-data-holder">
            <table>
                <tr class="up data-impressions"><td>750.000</td></tr>
                <tr class="down"><td>Impressions<span class="data-arrow"></span></td></tr>
            </table>
        </div>
    </div> <!--bit-data-->

    <div class="main-content-holder">

        <div class="page-header">
            <span>Summary: Listed below are all the ATM groups you have saved in Red Bar Media.</span>
        </div>

        <div class="subheading">
            <span>Location Owner / Manager.</span>
            <img class="subheading-icon" src="<?php echo base_url("img/icon_subheading_avatar.png") ?>" />
            <span class="subheading-shadow"></span>
        </div>

        <div class="white-canvas padding10">
            <button type="button" class="btn btn-default-custom">Submit</button>
            <button type="button" class="btn btn-warning-custom">Cancel</button>
        </div>

        <div class="left_half">
            <div class="subheading">
                <span>Location Owner / Manager.</span>
                <img class="subheading-icon" src="<?php echo base_url("img/icon_subheading_avatar.png") ?>" />
                <span class="subheading-shadow"></span>
            </div>
            <div class="white-canvas padding10">
                <button type="button" class="btn btn-default-custom">Submit</button>
                <button type="button" class="btn btn-warning-custom">Cancel</button>
            </div>
        </div>
        <div class="right_half">
            <div class="subheading">
                <span>Location Owner / Manager.</span>
                <img class="subheading-icon" src="<?php echo base_url("img/icon_subheading_avatar.png") ?>" />
                <span class="subheading-shadow"></span>
            </div>
            <div class="white-canvas padding10">
                <button type="button" class="btn btn-default-custom">Submit</button>
            </div>
            <div class="white-canvas">  

                <div class=" acc-holder">
                    <div id="accordion">
                        <div class="acc-item">
                            <div class="acc-item-options">
                                <button  type="button" class="btn btn-default-custom">Add ATM</button>
                                <span> &nbsp;|</span>
                                <div class="checkbox">  
                                    <input id="check1" type="checkbox" name="check" value="check1" checked>  
                                    <label for="check1">Map</label>
                                </div>
                            </div>
                            <div class="acc-trigger" style="border-left: 5px solid #c7282a">
                                <span>Section 1</span>
                            </div>
                            <p>Mauris mauris ante, blandit et, ultrices a, susceros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.</p>
                        </div>
                        <div class="acc-item">
                            <div class="acc-item-options">
                                <button  type="button" class="btn btn-default-custom">Add ATM</button>
                                <span> &nbsp;|</span>
                                <div class="checkbox">  
                                    <input id="check2" type="checkbox" name="check" value="check1" checked>  
                                    <label for="check2">Map</label>
                                </div>
                            </div>
                            <div class="acc-trigger" style="border-left: 5px solid #68B176">
                                <span>Section 2</span>
                            </div>
                            <div>
                                <table class="table acc-table">
                                    <tr class="acc-table-header">
                                        <td>ATM Location</td>
                                        <td>Address</td>
                                        <td>Type</td>
                                        <td>Options</td>
                                    </tr>
                                    <tr>
                                        <td>Location name</td>
                                        <td>Some cool address</td>
                                        <td>Eastern</td>
                                        <td class="options">
                                            <span class="edit"></span>
                                            <span class="delete"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ATM Location</td>
                                        <td>Address</td>
                                        <td>Type</td>
                                        <td class="options">
                                            <span class="edit"></span>
                                            <span class="delete"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>                       
                        <div class="acc-item">
                            <div class="acc-item-options">
                                <button  type="button" class="btn btn-default-custom">Add ATM</button>
                                <span> &nbsp;|</span>
                                <div class="checkbox">  
                                    <input id="check3" type="checkbox" name="check" value="check1" checked>  
                                    <label for="check3">Map</label>
                                </div>
                            </div>
                            <div class="acc-trigger" style="border-left: 5px solid #7E72B4">
                                <span>Section 3</span>
                            </div>
                            <p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui. </p>
                        </div>
                    </div>
                </div> <!--accordion-->

            </div>

            <div class="white-canvas padding10">

                <div class="radio">  
                    <input id="male" type="radio" name="gender" value="male" checked>  
                    <label for="male">Yes</label>  
                    <input id="female" type="radio" name="gender" value="female">  
                    <label for="female">No</label>  
                </div>  

                <div class="checkbox">  
                    <input id="check4" type="checkbox" name="check" value="check1" checked>  
                    <label for="check4">Map</label>
                </div>

            </div> <!--checkbox-and-radio-->


            <button type="button" class="btn btn-default-custom">Submit</button>
            <button type="button" class="btn btn-warning-custom">Cancel</button>


            <script>
                $(function() {
                    $("#accordion").accordion({header: ".acc-trigger", collapsible: true, active: false, heightStyle: "content"});
                });

                $(function() {
                    $("#accordion").sortable();
                    $("#accordion").disableSelection();
                });
            </script>
        </div>

        <div id="tabs" class="tabs-custom clear">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#home" role="tab" data-toggle="tab">Home</a></li>
                <li><a href="#profile" role="tab" data-toggle="tab">Profile</a></li>
                <li><a href="#messages" role="tab" data-toggle="tab">Messages</a></li>
                <li><a href="#settings" role="tab" data-toggle="tab">Settings</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="home">aaa</div>
                <div class="tab-pane" id="profile">sss</div>
                <div class="tab-pane" id="messages">ddd</div>
                <div class="tab-pane" id="settings">www</div>
            </div>
        </div> <!-- Nav tabs -->

        <script>
            $(function() {
                var tabs = $("#tabs").tabs();
                tabs.find(".ui-tabs-nav").sortable({
                    axis: "x",
                    stop: function() {
                        tabs.tabs("refresh");
                    }
                });
            });
        </script>

    </div><!-- /main-content-holder -->

</div><!-- /.container -->

