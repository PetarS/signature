<div class="container upload-container">    
    <div id="uploadbox" class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">                    

        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="panel-title">Please specify a file, or a set of files</div>
            </div>     

            <div class="panel-body" style="text-align: center;">


                <form class="form-horizontal login-form" role="form">

                    <div class="input-group mar-top">
                        <span class="input-group-btn">
                            <span class="upload btn btn-file">
                                *<input type="file" multiple>
                            </span>
                        </span>
                        <input type="text" class="form-control" disabled="disabled">
                    </div>
                    
                    <div class='clear'></div>

                    <div class="mar-top-l">
                        <a id="btn-login" href="#" class="btn btn-danger">Upload </a>
                        <span style="width: 20%; display: inline-block; margin-left: -8px;">or</span>
                        <a id="btn-login" href="#" class="btn btn-danger">List All Surveys </a>
                    </div> 
                </form> 
                
            </div>                     
        </div>  
    </div>

</div>

<script type="text/javascript">
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready(function() {
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                if (log)
                    alert(log);
            }

        });
    });
</script>


